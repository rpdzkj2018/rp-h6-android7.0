# Copyright (C) 2012 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
LOCAL_PATH := $(call my-dir)

#########################
include $(CLEAR_VARS)
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := king_policy

LOCAL_SHARED_LIBRARIES :=	libc \
							libz \
							liblog \
							libcutils \
							libdl

LOCAL_SRC_FILES :=  \
				    king_monitor.c \
					scene_policy.c \
					scene_taskset.c
					
LOCAL_C_INCLUDES := scene_policy.h \
LOCAL_C_INCLUDES += system/extras/tests/include 

LOCAL_CFLAGS += -DDEBUG_CACHE
LOCAL_INIT_RC := king_policy.rc
include $(BUILD_EXECUTABLE)

#########################
