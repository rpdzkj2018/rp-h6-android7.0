#ifndef __SCENE_POLICY_H__
#define __SCENE_POLICY_H__
#include <stdarg.h>
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <ctype.h>
#include <dirent.h>
#include <limits.h>
#include <sys/inotify.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/resource.h>
#include <sys/mman.h>
#include <inttypes.h>
#include <cutils/sched_policy.h>
#include <android/log.h>

#ifdef __LP64__
#define CPU_SETSIZE 1024
#else
#define CPU_SETSIZE 32
#endif

static int SCENE_POLICY_DEBUG_LEVEL = 0;

#define LOG_TAG "scene_policy"

#define B_ERR(fmt, args...)  if(SCENE_POLICY_DEBUG_LEVEL>=0) \
							__android_log_print(ANDROID_LOG_DEBUG,LOG_TAG, fmt, ##args)
#define B_INF(fmt, args...)  if (SCENE_POLICY_DEBUG_LEVEL>=1) \
							__android_log_print(ANDROID_LOG_DEBUG,LOG_TAG, fmt, ##args)
#define B_DEBUG(fmt,args...) if (SCENE_POLICY_DEBUG_LEVEL>=2) \
							__android_log_print(ANDROID_LOG_DEBUG,LOG_TAG, fmt, ##args)

#define RETFAULT		(-1)
#define RETFINE			(0)
#define POLICY_ENABLE		(1)
#define POLICY_DISABLE		(0)

typedef struct proc_software_sysctl {
	char scene_name[32];
	const char *inotify_item;
	const char *inotify_key;
	const char *triger_proc;
	int triger_pid;
	int item_event_mask;
	int key_event_mask;
#define INOTIFY_ITEM	0
#define INOTIFY_KEY		1
	int inotify_flag;
	int (*fn)(int, void*);
	int fn_int_para;
	void *fn_ptr_para;
}proc_sw_st;

#define GLORY_KING_POLICY	"king.glory.pref"
#define ANTUTU_POLICY		"benchmark.boost.pref"

/* cpu governors  */
/* interactive conservative ondemand userspace powersave performance */
typedef enum {
	PERFORMANCE  = 0,
	INTERACTIVE,
	GOVERNOR_MAX,
} cpu_governor_hw_st;

typedef struct cpu_hardware_sysctl {
	unsigned int cpu_minfreq;
	unsigned int cpu_maxfreq;
	unsigned int cpu_min_online;
	unsigned int cpu_max_online;
	unsigned int cpu_freq_governor;
}cpu_hw_st;

typedef struct gpu_hardware_sysctl {
	unsigned int gpu_freq;
	unsigned int gpu_performace;
}gpu_hw_st;

typedef struct thermal_hardware_sysctl {
	unsigned int enable;
	unsigned int adjust;
}the_hw_st;

typedef struct mem_hardware_sysctl {
	const char *zone_min_kbytes;
	const char *zone_extral_kbytes;
	const char *block_read_aheah_kbytes;
	const char *dirty_background_ratio;
	const char *dirty_ratio;
	const char *lmk_minfree;
}mem_hw_st;

typedef enum {
#define CPU_MAGIC       'c'
        CPU_MAGIC_SLOT = 0,
#define GPU_MAGIC       'g'
        GPU_MAGIC_SLOT,
#define THERMAL_MAGIC   't'
        THERMAL_MAGIC_SLOT,
#define MM_MAGIC        'm'
        MM_MAGIC_SLOT,
#define IDLE_MAGIC      'i'
        IDLE_MAGIC_SLOT,
#define EXT_MAGIC       'e'
        EXT_MAGIC_SLOT,
#define PRIO_MAGIC		'p'
		PRIO_MAGIC_SLOT,
// you can add more new magic
	MAX_MAGIC_SLOT,
}plat_hw_magic_slot;

typedef struct platform_hardware_slot {
	int pid;
	int slot_used;
        int slot_active[MAX_MAGIC_SLOT];
        int slot_value [MAX_MAGIC_SLOT];
}plat_hw_slot;

#define MAX_LINE 256
#define PROC_NAME_LEN 64
#define THREAD_NAME_LEN 32
#define POLICY_NAME_LEN 4
#define INIT_PROCS 50
#define THREAD_MULT 1

struct proc_info {
    struct proc_info *next;
    pid_t pid;
    pid_t tid;
    char name[PROC_NAME_LEN];
    char tname[THREAD_NAME_LEN];
    char state;
    uint64_t utime;
    uint64_t stime;
    char pr[3];
    long ni;
    uint64_t delta_utime;
    uint64_t delta_stime;
    uint64_t delta_time;
    uint64_t vss;
    uint64_t rss;
    int num_threads;
    char policy[POLICY_NAME_LEN];
};

#define CPU_GOVERNOR_SYS 		"/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"
#define CPU_MAXFREQ_SYS 			"/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq"
#define CPU_MINFREQ_SYS 			"/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"
#define CPU_HOTPLUG_ENABLE_SYS	"/sys/kernel/autohotplug/enable"
#define CPU_HOTPLUG_ENABLE		"1"
#define PROC_MEMINFO	"/proc/meminfo"
#define CPU_CORE_PATH			"/sys/devices/system/cpu"
#define CPU_ROOMAGE_SYS         "/sys/devices/soc/cpu_budget_cool/roomage"
#define EMMC_BLOCK  			"/sys/block/mmcblk0"
#define NAND_BLOCK  			"/sys/block/nandm"
#define NAND_BLOCK_UDISK_READ_AHEAD	"/sys/block/nandm/queue/read_ahead_kb"
#define NAND_BLOCK_SYSTEM_READ_AHEAD	"/sys/block/nandd/queue/read_ahead_kb"
#define EMMC_BLOCK_SYSTEM_READ_AHEAD	"/sys/block/mmcblk0/queue/read_ahead_kb"
#define MM_MINFREE_KBYTES   		"/proc/sys/vm/min_free_kbytes"
#define MM_EXTRA_FREE_KBYTES   	"/proc/sys/vm/extra_free_kbytes"
#define MM_DIRTY_RATIO      		"/proc/sys/vm/dirty_ratio"
#define MM_DIRTY_BACK_RATIO		"/proc/sys/vm/dirty_background_ratio"
#define MM_LMK_MINFREE           "/sys/module/lowmemorykiller/parameters/minfree"
#define GPU_SCENECTRL_SYS           "/sys/devices/gpu/scenectrl/command"
#define GPU_PERFERMENCE             "1"
#define GPU_NORMAL                  "0"
#define THS_CPU_ENABLE_SYS			"/sys/devices/virtual/thermal/thermal_zone0/mode"
#define THS_CPU_CUR_STATE			"/sys/class/thermal/cooling_device0/cur_state"
#define THS_CPU_CUR_TEMP			"sys/class/thermal/thermal_zone0/temp"
#define THS_GPU_ENABLE_SYS			"/sys/devices/virtual/thermal/thermal_zone1/mode"
#define THS_GPU_CUR_STATE			"/sys/class/thermal/cooling_device1/cur_state"
#define VE_SMP_AFFINITY			"/proc/irq/121/smp_affinity_list"
#define VE_IRQ_POLICY			"0xf"
#define GPU_IRQ_AFFINITY		"/proc/irq/115/smp_affinity_list"

extern int aw_sysctrl_get(const char *path, char *buf, int slen);
extern int aw_sysctrl_set(const char *path, const char *buf);
extern int aw_devctrl_set(const char *path, int data);
extern int aw_devctrl_get(const char *path, int *data);
extern int aw_set_roomage(const char *path, char *buf);
extern int aw_get_magic(char magic, int magic_value,  plat_hw_slot *slot);
extern int aw_get_para(const char *value, plat_hw_slot *hw_slot);
extern int aw_check_sysctrl_access(const char *path, int dentry);
extern int aw_search_process(const char *procs);
extern int aw_check_process(const char *procs, const char *name);
extern int aw_inotify_event(const char *path, int file_flag);
extern int aw_taskset(int pid, int mask);
extern int aw_get_meminfo(char *name, int length, int *value, char *units);
extern int aw_king_monitor_threads(pid_t pid);

#endif //__SCENE_POLICY_H__
