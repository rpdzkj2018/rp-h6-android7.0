#ifndef AW_IDET_H
#define AW_IDET_H

#include "vdecoder.h"

typedef int (*idet_filter_func)(const uint8_t *a, const uint8_t *b, const uint8_t *c, int w , int index);

typedef struct {
    int64_t         alpha0;
    int64_t         alpha1;
    int64_t         delta ;
    int64_t         gamma0;
    int64_t         gamma1;
}calcuValue;

typedef struct {
    VideoPicture*          pPic[3];
    idet_filter_func       filter_line;
    int                    nThread_number;
    int                    nIndex;
}IdetCtx;

IdetCtx*  detectorCreate(void);//create detector
int  detectorInit(IdetCtx* pIdet);//init
calcuValue doCalculate(IdetCtx* pIdet);//calculate value

#endif
